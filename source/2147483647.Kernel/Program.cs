﻿using System;

namespace _2147483647.Kernel
{
	class Program{
		static void Main(string[] args){
            Console.ForegroundColor = ConsoleColor.Red;
            Console.WriteLine("Kernel Panic");
            Console.WriteLine("0xFFFFFFFF: No CRC[Code-Reproduction-Code]");
            Console.ResetColor();
            Console.ReadKey();
        }
	}
}